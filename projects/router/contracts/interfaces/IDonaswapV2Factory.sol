// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./IDonaswapV2Exchange.sol";
import "../token/ERC20/IERC20.sol";

interface IDonaswapV2Factory {
    function getPair(IERC20 tokenA, IERC20 tokenB) external view returns (IDonaswapV2Exchange pair);
}