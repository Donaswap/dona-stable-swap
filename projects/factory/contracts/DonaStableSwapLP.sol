// SPDX-License-Identifier: MIT
pragma solidity ^0.8.10;

import "./token/ERC20/ERC20.sol";

contract DonaStableSwapLP is ERC20 {
    address public immutable minter;

    constructor() ERC20("Dona StableSwap LPs", "Stable-DSLP") {
        minter = msg.sender;
    }

    /**
     * @notice Checks if the msg.sender is the minter address.
     */
    modifier onlyMinter() {
        require(msg.sender == minter, "Not minter");
        _;
    }

    function mint(address _to, uint256 _amount) external onlyMinter {
        _mint(_to, _amount);
    }

    function burnFrom(address _to, uint256 _amount) external onlyMinter {
        _burn(_to, _amount);
    }
}