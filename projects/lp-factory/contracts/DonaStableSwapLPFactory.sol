// SPDX-License-Identifier: MIT
pragma solidity ^0.8.10;

import "./access/Ownable.sol";
import "./DonaStableSwapLP.sol";

contract DonaStableSwapLPFactory is Ownable {
    uint256 public constant N_COINS = 2;

    event NewStableSwapLP(address indexed swapLPContract, address indexed tokenA, address indexed tokenB);

    constructor() {}

    /**
     * @notice createSwapLP
     * @param _coins: Addresses of ERC20 conracts of coins (c-tokens) involved
     * @param _minter: Minter address
     */
    function createSwapLP(address[N_COINS] memory _coins, address _minter) external onlyOwner returns (address) {
        // create LP token
        bytes memory bytecode = type(DonaStableSwapLP).creationCode;
        bytes32 salt = keccak256(abi.encodePacked(_coins, msg.sender, block.timestamp, block.chainid));
        address lpToken;
        assembly {
            lpToken := create2(0, add(bytecode, 32), mload(bytecode), salt)
        }
        DonaStableSwapLP(lpToken).setMinter(_minter);
        emit NewStableSwapLP(lpToken, _coins[0], _coins[1]);
        return lpToken;
    }
}