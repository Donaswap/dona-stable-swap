const networks = {
  eth: 'eth',
  goerli: 'goerli',
  bscMainnet: 'bscMainnet',
  bscTestnet: 'bscTestnet',
  opBscTestnet: 'opBscTestnet',
  hardhat: 'hardhat',
}

let network = process.env.NETWORK
console.log(network, 'network')
if (!network || !networks[network]) {
  throw new Error(`env NETWORK: ${network}`)
}

await $`yarn workspace @donaswap/factory run hardhat run scripts/deploy.ts --network ${network}`

await $`yarn workspace @donaswap/lp-factory run hardhat run scripts/deploy.ts --network ${network}`

await $`yarn workspace @donaswap/router run hardhat run scripts/deploy.ts --network ${network}`

console.log(chalk.blue('Done!'))

const m = await fs.readJson(`./projects/factory/deployments//${network}/${network}.json`)
const r = await fs.readJson(`./projects/lp-factory/deployments/${network}/${network}.json`)
const c = await fs.readJson(`./projects/router/deployments/${network}/${network}.json`)

const addresses = {
  ...m,
  ...r,
  ...c,
}

console.log(chalk.blue('Writing to file...'))
console.log(chalk.yellow(JSON.stringify(addresses, null, 2)))

fs.writeJson(`./deployments/${network}/${network}.json`, addresses, { spaces: 2 })
