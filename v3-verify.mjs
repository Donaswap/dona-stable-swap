const networks = {
  eth: 'eth',
  goerli: 'goerli',
  bscMainnet: 'bscMainnet',
  bscTestnet: 'bscTestnet',
  opBscTestnet: 'opBscTestnet',
  hardhat: 'hardhat',
}

let network = process.env.NETWORK
console.log(network, 'network')
if (!network || !networks[network]) {
  throw new Error(`env NETWORK: ${network}`)
}

await $`yarn workspace @donaswap/factory run hardhat run scripts/verify.ts --network ${network}`

await $`yarn workspace @donaswap/lp-factory run hardhat run scripts/verify.ts --network ${network}`

await $`yarn workspace @donaswap/router run hardhat run scripts/verify.ts --network ${network}`

console.log(chalk.blue('Done!'))
