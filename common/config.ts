export const configs = {
    firechain: {
        _WETHAddress : '',
        _donaswapV2 : '',
        _stableswapFactory : '',
    },
    rinia: {
        _WETHAddress : '',
        _donaswapV2 : '',
        _stableswapFactory : '',
    },
    eth: {
        _WETHAddress : '',
        _donaswapV2 : '',
        _stableswapFactory : '',
    },
    goerli: {
        _WETHAddress : '',
        _donaswapV2 : '',
        _stableswapFactory : '',
    },
    sepolia: {
        _WETHAddress : '',
        _donaswapV2 : '',
        _stableswapFactory : '',
    },
    bsc: {
        _WETHAddress : '',
        _donaswapV2 : '',
        _stableswapFactory : '',
    },
    bscTestnet: {
        _WETHAddress : '',
        _donaswapV2 : '',
        _stableswapFactory : '',
    },
    opBnbTestnet: {
        _WETHAddress : '',
        _donaswapV2 : '',
        _stableswapFactory : '',
    },
    polygon: {
        _WETHAddress : '',
        _donaswapV2 : '',
        _stableswapFactory : '',
    },
    polygonMumbai: {
        _WETHAddress : '',
        _donaswapV2 : '',
        _stableswapFactory : '',
    },
}